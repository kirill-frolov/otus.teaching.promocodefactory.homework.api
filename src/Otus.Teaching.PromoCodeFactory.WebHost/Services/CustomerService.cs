﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using ProtoCustomerService;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomerService : customers.customersBase
    {
        private IRepository<Customer> _customerRepository;
        private IRepository<Preference> _preferenceRepository;

        public CustomerService(
             IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<CustomersListReply> GetCustomersAsync(EmptyMessage request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();
            var response = new CustomersListReply();
            foreach (var custormer in customers)
            {
                response.Customers.Add(custormer.Id.ToString(), new CustomerReplyShort
                {
                    FirstName = custormer.FirstName,
                    LastName = custormer.LastName,
                    Email = custormer.Email
                });
            }
            return response;
        }

        public override async Task<CustomerReply> GetCustomerAsync(StringValue request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Value, out Guid id))
                throw new InvalidCastException();

            var customer = await _customerRepository.GetByIdAsync(id);

            var response = new CustomerReply
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.Email,
            };
            customer.Preferences.ToList().ForEach(x => response.Preferences.Add(x.PreferenceId.ToString(), x.Preference.Name));
            return response;
        }

        public override async Task<CustomerReply> CreateCustomerAsync(CreateCustomerRequest request, ServerCallContext context)
        {
            var preferenceIds = request.Preferences.Select(x => new Guid(x.Key)).ToList();
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(preferenceIds);
            var customer = new Customer();
            customer.Id = Guid.NewGuid();
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
            await _customerRepository.AddAsync(customer);
            var response = new CustomerReply
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
            };

            foreach (var preference in customer.Preferences)
                response.Preferences.Add(preference.PreferenceId.ToString(), preference.Preference.Name);
            return response;
        }

        public override async Task<EmptyMessage> EditCustomersAsync(EditCustomerRequest request, ServerCallContext context)
        {
            if (Guid.TryParse(request.Id, out Guid id))
                throw new InvalidCastException();
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                throw new Exception("Entity not found");

            var preferenceIds = request.Preferences.Select(x => new Guid(x.Key)).ToList();
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(preferenceIds);

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
            await _customerRepository.UpdateAsync(customer);
            return new EmptyMessage();
        }

        public override async Task<EmptyMessage> DeleteCustomersAsync(StringValue request, ServerCallContext context)
        {
            if (Guid.TryParse(request.Value, out Guid id))
                throw new InvalidCastException();
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                throw new Exception("Entity not found");
            await _customerRepository.DeleteAsync(customer);
            return new EmptyMessage();

        }

    }
}
