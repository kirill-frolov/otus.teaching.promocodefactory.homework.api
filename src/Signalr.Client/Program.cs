﻿using Microsoft.AspNetCore.SignalR.Client;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Signalr.Client
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            var connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/hubs/customers")
                .WithAutomaticReconnect()
                .Build();

            await connection.StartAsync();
            var customers = await connection.InvokeAsync<List<CustomerShortResponse>>("GetCustomersAsync");
            var customer = await connection.InvokeAsync<CustomerResponse>("GetCustomerAsync", new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"));
            var createdCustomer = await connection.InvokeAsync<CustomerResponse>("CreateCustomerAsync", new CreateOrEditCustomerRequest
            {
                Email = "test@email.com",
                FirstName = "Test",
                LastName = "Test",
                PreferenceIds = new List<Guid> { new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c") }
            }) ;
            var editedCustomer = await connection.InvokeAsync<CustomerResponse>("EditCustomersAsync", createdCustomer.Id, 
                new CreateOrEditCustomerRequest
                {
                    Email = "test@email.com",
                    FirstName = "TestEdited",
                    LastName = "Test",
                    PreferenceIds = new List<Guid>()
                });
            await connection.InvokeAsync("DeleteCustomerAsync", createdCustomer.Id);
            await connection.StopAsync();
            Console.ReadKey();
        }
    }
}

